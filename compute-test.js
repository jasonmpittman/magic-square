/**
  * Magic-square (compute-test)
  * https://github.com/jasonmpittman/magic-square
  *
  * Copyright (c) 2016 Jason M. Pittman
  * Licensed under the MIT license
*/
const magicSquare = require('./index.js');

var myMagicSquare = magicSquare.compute();

for (var i = 0; i < 9; i++) {
  console.log(myMagicSquare[i]);
}
