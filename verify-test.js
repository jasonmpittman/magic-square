/**
  * Magic-square (verify-test)
  * https://github.com/jasonmpittman/magic-square
  *
  * Copyright (c) 2016 Jason M. Pittman
  * Licensed under the MIT license
*/
var verifier = require('./index.js');

var validMagicSquare = [2,7,6,9,5,1,4,3,8];
var invalidMagicSquare = [1,2,3,4,5,6,7,8];

if (verifier.verify(validMagicSquare)) {
  console.log("This is a valid magic square");
} else {
  console.log("This is not a valid magic square");
}
