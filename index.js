/**
  * Magic-square
  * https://github.com/jasonmpittman/magic-square
  *
  * Copyright (c) 2016 Jason M. Pittman
  * Licensed under the MIT license
*/

/**
  * Compute a 3x3 magic square
  * @return {array[]}
*/
module.exports = {
  compute: function() {
    /** compute Array(9) consisting of 1-9 randomly ordered
        may not be a valid magic square
    */
    var magicSquare = new Array(9);

    for (var i = 0; i < 9; i++) {
      var element = (Math.floor(Math.random() * 9) + 1);
      if (magicSquare.indexOf(element) == -1) {
        magicSquare[i] = element;
      } else {
        i--;
      }
    }
    return magicSquare;
  },

/**
  * Verify a 3x3 magic square
  * @param {array[]}
  * @return {boolean}
*/
  verify: function (array) {
    var isValidMagicSquare = false;

    rowOne = array[0] + array[1] + array[2];
    rowTwo = array[3] + array[4] + array[5];
    rowThree = array[6] + array[7] + array [8];
    colOne = array[0] + array[3] + array[6];
    colTwo = array[1] + array[4] + array[7];
    colThree = array[2] + array[5] + array[8];
    diagOne = array[0] + array[4] + array[8];
    diagTwo = array[2] + array[4] + array[6];

    if (rowOne == 15 && rowTwo == 15 && rowThree == 15 &&
      colOne == 15 && colTwo == 15 && colThree == 15 &&
      diagOne == 15 && diagTwo == 15) {
        isValidMagicSquare = true;
      }
      else {
        return isValidMagicSquare;
      }
    return isValidMagicSquare;
  }

};
